Para testar as funções deve-se utilizar a ferramenta Postman. "https://www.postman.com/downloads/"

>Dado uma lista de números, imprimir a lista de trás pra frente:
Passando como parametros "numbers"
Exemplo: GET localhost:8080/listaReversa?numbers=2&numbers=5&numbers=9
Retorno: [9, 5, 2]


>Dado uma lista de números, imprimir todos os numeros impares
Passando como parametros "numbers"
Exemplo: GET localhost:8080/imprimirImpares?numbers=2&numbers=5&numbers=9
Retorno: [5, 9]

>Dado uma lista de números, imprimir todos os numeros pares
Passando como parametros "numbers" 
Exemplo: GET localhost:8080/imprimirPares?numbers=2&numbers=5&numbers=9
Retorno: [2]

>Dado uma palavra, contar o tamanho dela 
Passando como parametro "palavra"
Exemplo: GET localhost:8080/tamanho?Palavra=desafio
Retorno: 7

>Dado uma lista de palavras, devolver todas em letras maisculas
Passando como parametro "palavra"
Exemplo: GET localhost:8080/maiusculas?Palavra=desafio
Retorno: "DESAFIO"

>Dado uma lista de palavras,devolver todas as vogais
Passando como parametro "palavra"
Exemplo: GET localhost:8080/vogais?Palavra=desafio
Retorno: ['e', 'a', 'i', 'o']

>Dado uma lista de palavras,devolver todas as consoantes
Passando como parametro "palavra"
Exemplo: GET localhost:8080/consoantes?Palavra=desafio
Retorno: ['d', 's', 'f']


>Dado um nome transformalo em nome bibliografico
Passando como parametro "nome"
Exemplo: GET localhost:8080/bibliografico?nome=Ana Marcia Goulart
Retorno:'GOULART, Ana Marcia'

>Sistema monetário
Passando como parametro "valor" de saque.
Exemplo: GET localhost:8080/SistemaMonetario?valor=11
Retorno: "Saque R$11.0: 2 Notas de R$3 e 1 Notas de R$5"

>Calculadora
Passando os parametros no body: 
[
	{
		"numerador": 10,
		"denominador": 5
	},
	{
		"numerador": 2,
		"denominador":8
	}
]

Exemplo: POST localhost:8080/divisaofracao
Retorno: 1/2
Para calculadora também existe os endpoints: 
/multiplicacaofracao
/somatoriofracao
/subtracaofracao
