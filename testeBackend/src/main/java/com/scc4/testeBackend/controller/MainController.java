package com.scc4.testeBackend.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.scc4.testeBackend.Util.FracaoUtil;
import com.scc4.testeBackend.model.Fracao;

@RestController
public class MainController {

	// Dado uma lista de números, imprimir a lista de trás pra frente.

	@GetMapping("/listaReversa")
	public ResponseEntity<?> inverterListaDeNumeros(Integer[] numbers) {

		int tamanho = numbers.length;
		Integer[] vetInvertido = new Integer[tamanho];
		for (int i = 0; i < tamanho; i++) {
			vetInvertido[tamanho - 1 - i] = numbers[i];

		}

		return new ResponseEntity<Integer[]>(vetInvertido, HttpStatus.OK);
	}

	// Dado uma lista de números, imprimir todos os numeros impares
	@GetMapping("/imprimirImpares")
	public ResponseEntity<?> numerosImpares(Integer[] numbers) {

		List<Integer> impar = new ArrayList();

		for (int i = 0; i < numbers.length; i++) {
			if (!(numbers[i] % 2 == 0)) {
				impar.add(numbers[i]);

			}
		}

		return new ResponseEntity<List<Integer>>(impar, HttpStatus.OK);

	}

	// Dado uma lista de números, imprimir todos os numeros pares.
	@GetMapping("/imprimirPares")
	public ResponseEntity<?> numerosPares(Integer[] numbers) {

		List<Integer> pares = new ArrayList();
		for (int i = 0; i < numbers.length; i++) {
			if ((numbers[i] % 2 == 0)) {
				pares.add(numbers[i]);

			}
		}

		return new ResponseEntity<List<Integer>>(pares, HttpStatus.OK);

	}

	// Dado uma palavra, contar o tamanho dela
	@GetMapping("/tamanho")
	public ResponseEntity<?> contarString(String palavra) {

		Integer tamanhoString = palavra.length();

		return new ResponseEntity<Integer>(tamanhoString, HttpStatus.OK);
	}

	// Dado uma lista de palavras, devolver todas em letras maisculas
	@GetMapping("/maiusculas")
	public ResponseEntity<?> UpperCase(String palavra) {

		String newPalavra = palavra.toUpperCase();

		return new ResponseEntity<String>(newPalavra, HttpStatus.OK);
	}

	// Dado uma lista de palavras,devolver todas as vogais
	@GetMapping("/vogais")
	public ResponseEntity<?> devolveVogais(String palavra) {
		String[] vogais = { "a", "e", "i", "o", "u" };

		List<String> vogaisDaPalavra = new ArrayList();

		for (int i = 0; i < palavra.length(); i++) {

			for (String vogal : vogais) {

				if (String.valueOf(palavra.charAt(i)).equalsIgnoreCase(vogal)) {
					vogaisDaPalavra.add(vogal);
				}

			}

		}

		return new ResponseEntity<List<String>>(vogaisDaPalavra, HttpStatus.OK);
	}

	// Dado uma lista de palavras,devolver todas as consoantes
	@GetMapping("/consoantes")
	public ResponseEntity<?> devolveConsoantes(String palavra) {
		String[] consoantes = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v",
				"w", "x", "y", "z" };

		List<String> consoantesDaPalavra = new ArrayList();

		for (int i = 0; i < palavra.length(); i++) {

			for (String consAtual : consoantes) {

				if (String.valueOf(palavra.charAt(i)).equalsIgnoreCase(consAtual)) {
					consoantesDaPalavra.add(consAtual);
				}

			}

		}

		return new ResponseEntity<List<String>>(consoantesDaPalavra, HttpStatus.OK);
	}

	// Dado um nome transformalo em nome bibliografico
	@GetMapping("/bibliografico")
	public ResponseEntity<?> nomeBibliografico(String nome) {

		String[] nomeVetor = nome.split(" ");

		StringBuilder nomeBibliografico = new StringBuilder(nomeVetor[nomeVetor.length - 1].toUpperCase()).append(",");
		for (int i = 0; i < nomeVetor.length - 1; i++) {
			nomeBibliografico.append(" ").append(nomeVetor[i]);
		}

		return new ResponseEntity<String>(nomeBibliografico.toString(), HttpStatus.OK);
	}

	// Sistema monetário >> Caixa eletronico com apenas notas de 5 e 3
	@GetMapping("/SistemaMonetario")
	public ResponseEntity<?> calcularCedulas(double valor) {

		Double valorOriginal = valor;
		Integer notas5 = 0;
		Integer notas3 = 0;

		if (valorOriginal.intValue() != valorOriginal) {
			return new ResponseEntity<String>("Impossível sacar este valor.", HttpStatus.OK);
		}

		while (valor > 0) {

			if (valor % 5 == 0) {
				valor -= 5;
				notas5++;
			} else if ((valor - 5) % 3 == 0) {
				valor -= 5;
				notas5++;
			} else {
				valor -= 3;
				notas3++;
			}

		}

		String resultado = "Saque R$" + valorOriginal + ": ";

		if (notas3 > 0) {
			resultado = resultado + notas3 + " Notas de R$3 ";
		}

		if (notas5 > 0) {

			if (notas3 > 0) {
				resultado = resultado + "e ";
			}
			resultado = resultado + notas5 + " Notas de R$5";
		}

		return new ResponseEntity<String>(resultado, HttpStatus.OK);
	}

	@PostMapping("/multiplicacaofracao")
	public ResponseEntity<?> multiplicarFracoes(@RequestBody List<Fracao> fracoes) {
		
		Fracao fracaoMultiplicacao = FracaoUtil.multiplicarFracoes(fracoes.get(0), fracoes.get(1));
		
		Map<String, String> response = new HashMap();
		response.put("Resultado", fracaoMultiplicacao.getNumerador() + "/" + fracaoMultiplicacao.getDenominador());
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/divisaofracao")
	public ResponseEntity<?> dividirFracoes(@RequestBody List<Fracao> fracoes) {
		
		Fracao fracaoMultiplicacao = FracaoUtil.dividirFracoes(fracoes.get(0), fracoes.get(1));
		
		Map<String, String> response = new HashMap();
		response.put("Resultado", fracaoMultiplicacao.getNumerador() + "/" + fracaoMultiplicacao.getDenominador());
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/somatoriofracao")
	public ResponseEntity<?> somarFracoes(@RequestBody List<Fracao> fracoes) {
		
		Fracao fracaoSoma = FracaoUtil.somarFracoes(fracoes.get(0), fracoes.get(1));
		
		Map<String, String> response = new HashMap();
		response.put("Resultado", fracaoSoma.getNumerador() + "/" + fracaoSoma.getDenominador());
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/subtracaofracao")
	public ResponseEntity<?> subtrairFracoes(@RequestBody List<Fracao> fracoes) {
		
		Fracao fracaoSoma = FracaoUtil.subtrairFracoes(fracoes.get(0), fracoes.get(1));
		
		Map<String, String> response = new HashMap();
		response.put("Resultado", fracaoSoma.getNumerador() + "/" + fracaoSoma.getDenominador());
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
	}

}