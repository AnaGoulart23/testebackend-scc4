package com.scc4.testeBackend.model;

public class Fracao {
	
    private Integer numerador;
    private Integer denominador;

    public Fracao(Integer numerador, Integer denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public Fracao() {
    	
    }
    
    public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}

	public void setDenominador(Integer denominador) {
		this.denominador = denominador;
	}


	public Integer getNumerador() {
		return numerador;
	}

	public Integer getDenominador() {
		return denominador;
	}

	@Override
    public String toString() {
        return new StringBuilder("Fração: ").append(numerador).append("/").append(denominador).toString();
    }

}
