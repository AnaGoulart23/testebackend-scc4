package com.scc4.testeBackend.Util;

import java.util.ArrayList;
import java.util.List;

import com.scc4.testeBackend.model.Fracao;

public class FracaoUtil {

	public static Fracao multiplicarFracoes(Fracao fracao1, Fracao fracao2) {
		Fracao resultado = new Fracao(fracao1.getNumerador() * fracao2.getNumerador(),
				fracao1.getDenominador() * fracao2.getDenominador());

		return simplificarFracao(resultado);
	}

	public static Fracao dividirFracoes(Fracao fracao1, Fracao fracao2) {
		Fracao resultado = new Fracao(fracao1.getNumerador() * fracao2.getDenominador(),
				fracao1.getDenominador() * fracao2.getNumerador());

		return simplificarFracao(resultado);
	}

	public static Fracao subtrairFracoes(Fracao fracao1, Fracao fracao2) {
		Integer mmc = calcularMmc(fracao1.getDenominador(), fracao2.getDenominador());

		Fracao novaFracao1 = criarFracaoPeloMmc(fracao1, mmc);
		Fracao novaFracao2 = criarFracaoPeloMmc(fracao2, mmc);

		Fracao resultado = new Fracao(novaFracao1.getNumerador() - novaFracao2.getNumerador(), mmc);

		return simplificarFracao(resultado);
	}

	public static Fracao somarFracoes(Fracao fracao1, Fracao fracao2) {
		Integer mmc = calcularMmc(fracao1.getDenominador(), fracao2.getDenominador());

		Fracao novaFracao1 = criarFracaoPeloMmc(fracao1, mmc);
		Fracao novaFracao2 = criarFracaoPeloMmc(fracao2, mmc);

		Fracao resultado = new Fracao(novaFracao1.getNumerador() + novaFracao2.getNumerador(), mmc);

		return simplificarFracao(resultado);
	}

	private static Fracao criarFracaoPeloMmc(Fracao fracao, Integer mmc) {
		Integer novoNumerador = mmc / fracao.getDenominador() * fracao.getNumerador();
		Fracao novaFracao = new Fracao(novoNumerador, mmc);

		return novaFracao;
	}

	private static Integer calcularMmc(Integer denominador1, Integer denominador2) {
		List<Integer> divisores = new ArrayList();

		while ((denominador1 != 1) || (denominador2 != 1)) {

			if (denominador1 % 7 == 0 || denominador2 % 7 == 0) {

				if (denominador1 % 7 == 0) {
					denominador1 = denominador1 / 7;
				}

				if (denominador2 % 7 == 0) {
					denominador2 = denominador2 / 7;
				}

				divisores.add(7);
			} else if (denominador1 % 5 == 0 || denominador2 % 5 == 0) {

				if (denominador1 % 5 == 0) {
					denominador1 = denominador1 / 5;
				}

				if (denominador2 % 5 == 0) {
					denominador2 = denominador2 / 5;
				}

				divisores.add(5);
			} else if (denominador1 % 3 == 0 || denominador2 % 3 == 0) {

				if (denominador1 % 3 == 0) {
					denominador1 = denominador1 / 3;
				}

				if (denominador2 % 3 == 0) {
					denominador2 = denominador2 / 3;
				}

				divisores.add(3);
			} else if (denominador1 % 2 == 0 || denominador2 % 2 == 0) {

				if (denominador1 % 2 == 0) {
					denominador1 = denominador1 / 2;
				}

				if (denominador2 % 2 == 0) {
					denominador2 = denominador2 / 2;
				}

				divisores.add(2);
			}
		}

		Integer mmc = 1;
		for (Integer divisor : divisores) {
			mmc = mmc * divisor;
		}

		return mmc;
	}

	public static Fracao simplificarFracao(Fracao fracao) {
		int i = 1;
		while (i == 1) {

			if ((fracao.getDenominador() % 7 == 0) && (fracao.getNumerador() % 7 == 0)) {
				fracao.setDenominador(fracao.getDenominador() / 7);
				fracao.setNumerador(fracao.getNumerador() / 7);
				System.out.println(fracao.getDenominador());

			} else if ((fracao.getDenominador() % 5 == 0) && (fracao.getNumerador() % 5 == 0)) {
				fracao.setDenominador(fracao.getDenominador() / 5);
				fracao.setNumerador(fracao.getNumerador() / 5);
				System.out.println(fracao.getDenominador());

			} else if ((fracao.getDenominador() % 3 == 0) && (fracao.getNumerador() % 3 == 0)) {
				fracao.setDenominador(fracao.getDenominador() / 3);
				fracao.setNumerador(fracao.getNumerador() / 3);
				System.out.println(fracao.getDenominador());

			} else if ((fracao.getDenominador() % 2 == 0) && (fracao.getNumerador() % 2 == 0)) {
				fracao.setDenominador(fracao.getDenominador() / 2);
				System.out.println(fracao.getDenominador());
				fracao.setNumerador(fracao.getNumerador() / 2);
				System.out.println(fracao.getNumerador());

			} else {
				break;
			}
		}
		
		return fracao;
	}

}
